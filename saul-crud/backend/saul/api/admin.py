from django import forms
from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.core.exceptions import ValidationError

from .models import (
    User,
    AdditonalEmail,
    Province,
    Canton,
    District,
    University,
    DegreeGrade,
    Degree,
    StudyArea,
)
class Emailsline(admin.TabularInline):
    model = AdditonalEmail

class UserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)
    class Meta:
        model = User
        fields = '__all__'

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """
    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = '__all__'
       
    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # # The fields to be used in displaying the User model.
    # # These override the definitions on the base UserAdmin
    # # that reference specific fields on auth.User.
    # list_display = ('email', 'date_of_birth', 'is_admin')
    # list_filter = ('is_admin',)

    inlines = [
        Emailsline,
    ]
    fieldsets = (
        (None, {'fields': ('username', 'email', 'password')}),
        ('Personal info', {'fields': (
            'first_name',
            'middle_name',
            'last_name',
            'second_last_name',
            'birthday',
            'goverment_id',
            'university_id',
            'phonenumber',
            'district',
            'degree',
        )}),
        ('Permissions', {'fields': ('is_staff', 'is_superuser',)}),
    )
    # # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': (
                'username',
                'password1',
                "password2",
                'first_name',
                'middle_name',
                'last_name',
                'second_last_name',
                'email',
                'goverment_id',
                'university_id',
                'birthday',
                'phonenumber',
                'district',
                'degree',
            )
        }),
    )
    # search_fields = ('email',)
    # ordering = ('email',)
    # filter_horizontal = ()

# Register your models here.

admin.site.register(User, UserAdmin)
admin.site.register(AdditonalEmail)
admin.site.register(Province)
admin.site.register(Canton)
admin.site.register(District)
admin.site.register(University)
admin.site.register(DegreeGrade)
admin.site.register(Degree)
admin.site.register(StudyArea)
