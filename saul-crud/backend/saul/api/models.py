from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db import models
from phonenumber_field.modelfields import PhoneNumberField


class Province(models.Model):
    code = models.CharField("Province's code", max_length=1, primary_key=True)
    name = models.CharField("Name of the province", max_length=150)

    def __str__(self):
        return self.name


class Canton(models.Model):
    code = models.CharField("Province's code", max_length=6, primary_key=True)
    name = models.CharField("Name of the canton", max_length=150)
    province = models.ForeignKey(Province, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name}, {self.province}"


class District(models.Model):
    code = models.CharField("District's code", max_length=6, primary_key=True)
    name = models.CharField("Name of the district", max_length=150)
    canton = models.ForeignKey(Canton, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} de {self.canton}"


class University(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        verbose_name = "University"
        verbose_name_plural = "Universities"

    def __str__(self):
        return self.name


class DegreeGrade(models.Model):
    name = models.CharField(max_length=100)
    acronym = models.CharField(max_length=50)

    def __str__(self):
        return self.name

class StudyArea(models.Model):
    name = models.CharField("Study Area's name", max_length=50)

class Degree(models.Model):
    study_area = models.OneToOneField(StudyArea, on_delete=models.CASCADE)
    degree_grade = models.ForeignKey(DegreeGrade, on_delete=models.CASCADE)
    university = models.ForeignKey(University, on_delete=models.CASCADE)
    concluded = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.degree_grade.acronym} en {self.area}"


class User(AbstractUser):
    middle_name = models.CharField("middle name", max_length=150, blank=True)
    second_last_name = models.CharField("second last name", max_length=150, blank=True)
    goverment_id = models.CharField("goverment issued ID", max_length=150, unique=True)
    university_id = models.CharField("university issued ID", max_length=150, blank=True)
    birthday = models.DateField("date of birth", null=True)
    phonenumber = PhoneNumberField("Phone number", blank=True)
    district = models.ForeignKey(District, on_delete=models.CASCADE, null=True)
    degree = models.ManyToManyField(Degree)

    REQUIRED_FIELDS = ["goverment_id"]

    def __str__(self):
        return self.username


class AdditonalEmail(models.Model):
    email = models.EmailField("email", max_length=255, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Additional Email"
        verbose_name_plural = "Additional Emails"

    def __str__(self):
        return self.email
