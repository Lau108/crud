from rest_framework.serializers import ModelSerializer
from .models import *

class ProvinceSerializer(ModelSerializer):
    class Meta:
        model = Province
        fields = ['code', 'name']

class CantonSerializer(ModelSerializer):
    class Meta:
        model = Canton
        fields = ['code', 'name', 'province']

class DistrictSerializer(ModelSerializer):
    class Meta:
        model = District
        fields = ['code', 'name', 'canton']

class UniversitySerializer(ModelSerializer):
    class Meta:
        model = University
        fields = ['name']

class DegreeGradeSerializer(ModelSerializer):
    class Meta:
        model = DegreeGrade
        fields = ['name', 'acronym']

class StudyAreaSerializer(ModelSerializer):
    class Meta:
        model = StudyArea
        fields = ['name']

class DegreeSerializer(ModelSerializer):
    class Meta:
        model = Degree
        fields = ['study_area', 'degree_grade', 'university', 'concluded']

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['middle_name', 'second_last_name', 'government_id',
                'university_id', 'birthday', 'phonenumber', 'district',
                'degree']

class AdditonalEmailSerializer(ModelSerializer):
    class Meta:
        model = AdditonalEmail
        fields = ['email', 'user']
