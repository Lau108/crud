from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from saul.api import views

urlpatterns = [
    path('api/province/', views.ProvinceList.as_view()),
    path('api/province/<int:pk>/', views.ProvinceDetail.as_view()),
    path('api/canton/', views.CantonList.as_view()),
    path('api/canton/<int:pk>/', views.CantonList.as_view()),
    path('api/district/', views.DistrictList.as_view()),
    path('api/district/<int:pk>/', views.DistrictList.as_view()),
    path('api/university/', views.UniversityList.as_view()),
    path('api/university/<int:pk>/', views.UniversityList.as_view()),
    path('api/degree_grade/', views.DegreeGradeList.as_view()),
    path('api/degree_grade/<int:pk>/', views.DegreeGradeList.as_view()),
    path('api/study_area/', views.StudyAreaList.as_view()),
    path('api/study_area/<int:pk>/', views.StudyAreaList.as_view()),
    path('api/degree/', views.DegreeList.as_view()),
    path('api/degree/<int:pk>/', views.DegreeList.as_view()),
    path('api/user/', views.UserList.as_view()),
    path('api/user/<int:pk>/', views.UserList.as_view()),
    path('api/email/', views.AdditonalEmailList.as_view()),
    path('api/email/<int:pk>/', views.AdditonalEmailList.as_view()),
]
