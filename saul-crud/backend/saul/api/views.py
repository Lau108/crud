#from django.shortcuts import render, redirect
#from django.http import JsonResponse
from rest_framework.viewsets import generics
from .models import *
from .serializers import *


class ProvinceList(generics.ListCreateAPIView):
    queryset = Province.objects.all()
    serializer_class = ProvinceSerializer

class CantonList(generics.ListCreateAPIView):
    queryset = Canton.objects.all()
    serializer_class = CantonSerializer

class DistrictList(generics.ListCreateAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer

class UniversityList(generics.ListCreateAPIView):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer

class DegreeGradeList(generics.ListCreateAPIView):
    queryset = DegreeGrade.objects.all()
    serializer_class = DegreeGradeSerializer

class StudyAreaList(generics.ListCreateAPIView):
    queryset = StudyArea.objects.all()
    serializer_class = StudyAreaSerializer

class DegreeList(generics.ListCreateAPIView):
    queryset = Degree.objects.all()
    serializer_class = DegreeSerializer

class UserList(generics.ListCreateAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class AdditonalEmailList(generics.ListCreateAPIView):
    queryset = AdditonalEmail.objects.all()
    serializer_class = AdditonalEmailSerializer


class ProvinceDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Province.objects.all()
    serializer_class = ProvinceSerializer

class CantonDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Canton.objects.all()
    serializer_class = CantonSerializer

class DistrictDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = District.objects.all()
    serializer_class = DistrictSerializer

class UniversityDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = University.objects.all()
    serializer_class = UniversitySerializer

class DegreeGradeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = DegreeGrade.objects.all()
    serializer_class = DegreeGradeSerializer

class StudyAreaDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StudyArea.objects.all()
    serializer_class = StudyAreaSerializer

class DegreeDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Degree.objects.all()
    serializer_class = DegreeSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = User.objects.all()
    serializer_class = UserSerializer

class AdditonalEmailDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = AdditonalEmail.objects.all()
    serializer_class = AdditonalEmailSerializer
